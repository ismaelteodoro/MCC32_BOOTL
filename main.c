/*******************************************************************************
Copyright 2016 Microchip Technology Inc. (www.microchip.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

To request to license the code under the MLA license (www.microchip.com/mla_license), 
please contact mla_licensing@microchip.com
*******************************************************************************/

/** INCLUDES *******************************************************/
#include "system.h"


#include "app_led_usb_status.h"

#include "usb.h"
#include "usb_device.h"


/********************************************************************
 * Function:        void main(void)
 *
 * PreCondition:    IN project pic32blink-Main add this 
 * project ( proprerties ->loading MCC32_BOOTL)
 * Set pic32blink-Main to main project on mplabX
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        Main program entry point.
 *
 * Note:            None
 *******************************************************************/
#define TIMEOUTTIMEOUT 100000
extern void appFunction(void);
extern int readMem;
extern int sendTXUart;
unsigned int myvar;
unsigned int timeout;
unsigned int addRead;

MAIN_RETURN main(void)
{
    unsigned int i;
    SYSTEM_Initialize(SYSTEM_STATE_USB_START);
    timeout = TIMEOUTTIMEOUT;
    addRead = 0x9D005000;
    USBDeviceInit();
    USBDeviceAttach();
    TRISBCLR = ( 1 << 7);
    asm("nop");
    asm("nop");
    asm("nop");
    asm("nop");
    asm("nop");

    asm("nop");
    asm("nop");
   
    while(1)
    {
        //LATBINV = (1 << 7);
        SYSTEM_Tasks();

        #if defined(USB_POLLING)
            // Interrupt or polling method.  If using polling, must call
            // this function periodically.  This function will take care
            // of processing and responding to SETUP transactions
            // (such as during the enumeration process when you first
            // plug in).  USB hosts require that USB devices should accept
            // and process SETUP packets in a timely fashion.  Therefore,
            // when using polling, this function should be called
            // regularly (such as once every 1.8ms or faster** [see
            // inline code comments in usb_device.c for explanation when
            // "or faster" applies])  In most cases, the USBDeviceTasks()
            // function does not take very long to execute (ex: <100
            // instruction cycles) before it returns.
            USBDeviceTasks();
        #endif

        //Application specific tasks
        APP_DeviceCustomHIDTasks();
        if(--timeout == 0)
        {
            timeout = TIMEOUTTIMEOUT;
        
            if(sendTXUart != 0)
            {
                sendTXUart--;
                if((unsigned int)sendTXUart > 100){
                    sendTXUart = 0;
                }
                switch(sendTXUart)
                {
                    case 3:
                    UART1_Write((uint8_t)(myvar>>0));
                    break;
                    case 2:
                    UART1_Write((uint8_t)(myvar>>8));
                    break;
                    case 1:
                    UART1_Write((uint8_t)(myvar>>16));
                    break;
                    case 0:
                    UART1_Write((uint8_t)(myvar>>24));
                    break;
                }
            }
        }
    }//end while
}//end main

/*******************************************************************************
 End of File
*/
